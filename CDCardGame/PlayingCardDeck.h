//
//  PlayingCardDeck.h
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
/** PlayingCardDeck 继承于Deck 是游戏中使用的牌堆模型*/

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
