//
//  PlayingCard.h
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
/** PlayingCard 继承于Card 游戏里面主要用到PlayingCard作为牌的模型类*/

#import "Card.h"

@interface PlayingCard : Card
/** 牌的花色*/
@property (nonatomic, strong) NSString *suit;
/** 牌的大小*/
@property (nonatomic, assign) NSUInteger rank;

/** 设置牌花色*/
+ (NSArray *)validSuit;
+ (NSUInteger)maxRank;
@end
