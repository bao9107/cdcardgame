//
//  Card.m
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
//

#import "Card.h"

@implementation Card
- (int)match:(NSArray *)cards
{
    int score = 0;
    for (Card *card in cards) {
        if ([card.contents isEqualToString:self.contents]) {
            score = 1;
        }
    }
    return score;
}
@end
