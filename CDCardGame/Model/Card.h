//
//  Card.h
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, assign, getter=isChosen) BOOL chosen;
@property (nonatomic, assign, getter=isMatched) BOOL matched;

- (int)match:(NSArray *)cards;
@end
