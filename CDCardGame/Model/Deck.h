//
//  Deck.h
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

/** 加牌*/
- (void)addCard:(Card *)card atTop:(BOOL)atTop;
- (void)addCard:(Card *)card;
/** 随机抽取牌*/
- (Card *)drawRandomCard;

@end
