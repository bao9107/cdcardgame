//
//  PlayingCardDeck.m
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
//

#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@implementation PlayingCardDeck
//初始化方法
- (instancetype)init
{
    if (self = [super init]) {
        for (NSString *suit in [PlayingCard validSuit]) {
            for (NSUInteger rank = 1; rank <= [PlayingCard maxRank]; rank++) {
                PlayingCard *card = [[PlayingCard alloc] init];
                card.suit = suit;
                card.rank = rank;
                [self addCard:card];
            }
        }
    }
    return self;
}

@end
