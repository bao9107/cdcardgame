//
//  PlayingCard.m
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard
// idea: 当在.m里面同时重写了属性的setter和getter时，就必须加上@synthesize自己生成带下划线的变量
@synthesize suit = _suit;

- (NSString *)contents
{
//   idea:
//    1）当rank为0时，即[0]为数组不确定数，用？标识
//    2）如何用1 ~ 13 表示 1 ~ 10，J，Q，K ？==》定义一个存放特殊字符的数组，rank作为数组下标
    NSArray *rankChar = [PlayingCard rankString];
    return [rankChar[self.rank] stringByAppendingString:self.suit];
}

// idea: 当suit为nil时 ==》重写getter方法，设置返回“？”
- (NSString *)suit
{
    return _suit ? _suit : @"?";
}

// idea: 限制设置suit只为四种花色 ==》重写setter方法
- (void)setSuit:(NSString *)suit
{
    if ([[PlayingCard validSuit] containsObject:suit]) {
        _suit = suit;
    }
}

// idea: 限制rank的设置条件
- (void)setRank:(NSUInteger)rank
{
    if (rank <= [PlayingCard maxRank]) {
        _rank = rank;
    }
}

+ (NSArray *)validSuit
{
    return @[@"♥",@"♦",@"♠",@"♣"];
}

+ (NSArray *)rankString
{
    return @[@"?",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+ (NSUInteger)maxRank
{
    return [PlayingCard rankString].count - 1;
}
@end
