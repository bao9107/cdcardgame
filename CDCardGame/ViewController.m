//
//  ViewController.m
//  CDCardGame
//
//  Created by coco on 15/1/16.
//  Copyright (c) 2015年 netease. All rights reserved.
//

#import "ViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *countLab;
@property (nonatomic, assign) NSUInteger countNum;
@property (nonatomic, strong) Deck *deck;

@end

@implementation ViewController
- (Deck *)deck
{
    if (!_deck) _deck = [[PlayingCardDeck alloc] init];
    return _deck;
}

- (IBAction)cardOnClick:(UIButton *)sender {
    if (sender.currentTitle.length) {
        [sender setBackgroundImage:[UIImage imageNamed:@"cardBack"] forState:UIControlStateNormal];
        [sender setTitle:@"" forState:UIControlStateNormal];
    } else {
        Card *randomCard = [self.deck drawRandomCard];
        if (randomCard) {
            [sender setBackgroundImage:[UIImage imageNamed:@"cardFore"] forState:UIControlStateNormal];
            [sender setTitle:randomCard.contents forState:UIControlStateNormal];
            self.countNum++;
        }
    }
}

- (void)setCountNum:(NSUInteger)countNum
{
    _countNum = countNum;
    self.countLab.text = [NSString stringWithFormat:@"count %lu",self.countNum];
}
@end
